﻿$(document).ready(function () {
    $("#SearchStartCreateTime").datepicker();
    $("#SearchEndCreateTime").datepicker();
    GetDeal();
});

function GetDeal() {
    $.ajax({
        url: "/Admin/Chart/" + url + "?SearchStartCreateTime=" + $("#SearchStartCreateTime").val() + "&SearchEndCreateTime=" + $("#SearchEndCreateTime").val(),
        type: "post",
        contentType: "application/json;charset=utf-8",
        beforeSend: function () {
            $("#btnSearch").off("click");
        },
        dataType: "json",
        success: function (datas) {
            $("#btnSearch").on("click", GetDeal);
            var title = [];
            var foot = [];
            var data = [];
            var index = 0;
            $(datas).each(function () {
                title.push(this.Title);
                var list = [];
                $(this.data).each(function () {
                    if (index == 0) {
                        foot.push(this.Key); //X轴只取一次
                    }
                    list.push(this.Value);
                });
                data.push({
                    name: this.Title,
                    type: 'bar',
                    data: list
                });
                index++;
            });
            var myChart = echarts.init(document.getElementById('chart_bars_vertical'));
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                legend: {
                    data: title
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: foot
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        splitNumber: 1
                    }
                ],
                series: data
            };
            var show = false;
            $(data).each(function (i,v) {
                if (v.data.length > 0) {
                    show = true;
                    return;
                }
            });
            // 使用刚指定的配置项和数据显示图表。
            if (show) {
                myChart.setOption(option);
            } else {
                myChart.showLoading({
                    text: '暂无数据',
                    effect: 'bubble',
                    textStyle: {
                        fontSize: 30
                    }
                });
            }
        }
    });
}