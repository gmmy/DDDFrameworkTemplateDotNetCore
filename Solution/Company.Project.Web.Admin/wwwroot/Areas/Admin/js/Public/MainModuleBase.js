﻿function InitLeftMenu(_menus, AreaName, ModuleName, ModuleActionName) {
    var menulist = "";
    var showController = "";
    var showArea = "";
    $.each(_menus.menus, function (i, n) {
        if (n.menuname == AreaName) {
            showArea = n.menuid;
            menulist += "<li class=\"open active\">";
        } else {
            showArea = "";
            menulist += "<li class=\"\">";
        }
        menulist += "<a href=\"" + (n.url.length > 0 ? n.url : "javascript:;") + "\" class=\"dropdown-toggle\">";
        menulist += "<i class=\"menu-icon fa " + (n.modulecls) + "\"></i>";
        menulist += "<span class=\"menu-text\">";
        menulist += n.menuname;
        menulist += "</span>";
        menulist += "<b class=\"arrow fa fa-angle-down\"></b>";
        menulist += "</a>";
        menulist += "<b class=\"arrow\"></b>";
        if (n.menus && n.menus.length > 0) {
            menulist += "<ul class=\"submenu\">";

            $.each(n.menus, function (j, o) {
                if (showArea != "" && o.menuname == ModuleName) {
                    showController = o.menuid;
                    menulist += "<li class=\"active\">";
                } else {
                    if (o.menuname == AreaName) {
                        showArea = o.menuid;
                        menulist += "<li class=\"active getfatheropen\">";
                    } else {
                        showController = "";
                        menulist += "<li class=\"\">";
                    }
                }
                if (o.ismodule) {

                    menulist += "<a href=\"" + (o.url.length > 0 ? o.url : "javascript:;") + "\" class=\"" + (o.url.length > 0 ? "" : "dropdown-toggle") + "\">";

                    menulist += "<i class=\"menu-icon fa fa-caret-right\"></i>";
                    menulist += o.menuname;
                    if ((o.url.length > 0 ? false : true)) {
                        menulist += "<b class=\"arrow fa fa-angle-down\"></b>";
                    }
                    menulist += "</a>";
                    menulist += "<b class=\"arrow\"></b>";
                    if (o.menus && o.menus.length > 0) {
                        menulist += "<ul class=\"submenu\">";
                        $.each(o.menus, function (l, k) {
                            if (showController != "" && o.menuname == ModuleActionName) {
                                menulist += "<li class=\"active getfatheropen\">";
                            } else {
                                if (k.menuname == ModuleName) {
                                    menulist += "<li class=\"active getfatheropen\">";
                                    showController = k.menuid;
                                } else {
                                    menulist += "<li class=\"\">";
                                }
                            }
                            menulist += "<a href=\"" + (k.url.length > 0 ? k.url : k.menus.length == 1 ? k.menus[0].url : "javascript:;") + "\" class=\"" + (k.url.length > 0 ? "" : k.menus.length > 1 ? "dropdown-toggle" : "") + "\">";
                            menulist += "<i class=\"menu-icon fa fa-caret-right\"></i>";
                            menulist += k.menuname;
                            menulist += "</a>";
                            menulist += "<b class=\"arrow\"></b>";
                            if (k.menus.length > 1) {
                                menulist += "<ul class=\"submenu\">";
                                $.each(k.menus, function (q, r) {
                                    if (showController != "" && r.menuname == ModuleActionName) {
                                        menulist += "<li class=\"active getfatheropen\">";
                                    } else {
                                        menulist += "<li class=\"\">";
                                    }
                                    menulist += "<a href=\"" + (r.url.length > 0 ? r.url : "javascript:;") + "\">";
                                    menulist += "<i class=\"menu-icon fa fa-caret-right\"></i>";
                                    menulist += r.menuname;
                                    menulist += "</a>";
                                    menulist += "<b class=\"arrow\"></b>";
                                    menulist += "</li>";
                                });
                                menulist += "</ul>";
                            }
                            menulist += "</li>";
                        });
                        menulist += "</ul>";
                    }
                    menulist += "</li>";
                } else {
                    if ((o.url.length > 0 || (o.menus && o.menus.length > 0))) { //有且仅有设置了地址或者还有可在列表展示子级的情况下，才显示当前父级
                        menulist += "<a href=\"" + (o.url.length > 0 ? o.url : o.menus.length == 1 ? o.menus[0].url : "javascript:;") + "\" class=\"" + (o.url.length > 0 ? "" : o.menus.length > 1 ? "dropdown-toggle" : "") + "\">";

                        menulist += "<i class=\"menu-icon fa fa-caret-right\"></i>";
                        menulist += o.menuname;
                        if ((o.url.length > 0 ? false : o.menus.length <= 1 ? false : true)) {
                            menulist += "<b class=\"arrow fa fa-angle-down\"></b>";
                        }
                        menulist += "</a>";
                        menulist += "<b class=\"arrow\"></b>";
                        if (o.menus.length > 1) {
                            menulist += "<ul class=\"submenu\">";
                            $.each(o.menus, function (l, k) {
                                if (showController != "" && k.menuname == ModuleActionName) {
                                    menulist += "<li class=\"active getfatheropen\">";
                                } else {
                                    menulist += "<li class=\"\">";
                                }
                                menulist += "<a href=\"" + (k.url.length > 0 ? k.url : "javascript:;") + "\">";
                                menulist += "<i class=\"menu-icon fa fa-caret-right\"></i>";
                                menulist += k.menuname;
                                menulist += "</a>";
                                menulist += "<b class=\"arrow\"></b>";
                                menulist += "</li>";
                            });
                            menulist += "</ul>";
                        }
                        menulist += "</li>";
                    }
                }
            });
            menulist += "</ul>";
        }
        menulist += "</li>";
    });
    $("#menu").append(menulist);
    $("#menu li[class*='getfatheropen']").each(function () {
        if ($(this).find("ul").length == 0) {
            $(this).prop("class", "active");
            $(this).parents("li").prop("class", "open active");
        }
    });
    $(document).on("click", "#menu a", function () {
        var url = $(this).prop("href");
        if (url != "javascript:;") {
            $("#mainFrame").prop("src", url);
            //加载iframe后删除之前的topFun记录+首页的测试demo记录并且重置my-modal-info内容(方便他人重写)
            $("#childFun").html("");
            $("#modaltest").empty();
            $("#my-modal-info .modal-content").html("<form class=\"form-horizontal\" enctype=\"multipart/form-data\" id=\"formDetail\" role=\"form\"><div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button><h3 class=\"smaller lighter blue no-margin\">详细信息</h3></div><div class=\"modal-body\"></div><div class=\"modal-footer\"><a class=\"btn btn-sm btn-success pull-right\" id=\"saveBtn\"><i class=\"ace-icon fa fa-check\"></i>保存</a></div></form>");
        }
        return false;
    });
}

function TipMsg(msg) {
    bootbox.alert(msg);
}
function TipMsg(msg, callback) {
    bootbox.alert(msg, function () {
        if (callback)
            $.Callbacks().add(callback).fire();
    });
}
function TipMsg(msg, callback, parm) {
    bootbox.alert(msg, function () {
        if (callback)
            $.Callbacks().add(callback).fire(parm);
    });
}
function TipConfirm(msg, callback) {
    bootbox.confirm(msg, function (e) {
        if (callback && e)
            $.Callbacks().add(callback).fire();
    });
}
function TipConfirm(msg, callback, parm) {
    bootbox.confirm(msg, function (e) {
        if (callback && e)
            $.Callbacks().add(callback).fire(parm);
    });
}
function loadajaxSelect(classid, callback, parm) {
    $(classid).each(function () {
        var obj = $(this);
        if (obj.attr("ajax-url").length > 0) {
            $.ajax({
                url: obj.attr("ajax-url"),
                type: "get",
                dataType: "json",
                async: false,
                success: function (data) {
                    var option = "";
                    $(data).each(function (i, e) {
                        option += "<option " + (e.selected == true ? "selected=\"selected\"" : "") + " value=\"" + e.value + "\">" + e.text + "</option>";
                    });
                    obj.html(option);
                    if (callback && parm)
                        $.Callbacks().add(callback).fire(parm);
                    else if (callback)
                        $.Callbacks().add(callback).fire();
                },
                error: function (e) {
                    TipMsg("下拉列表加载失败,请稍后再试!");
                }
            });
        }
    });
}

function loadImageUpload(classid, arrayContainerId, isReset) {
    var postData = [];
    $.each(arrayContainerId, function (n, value) {
        if ($(value).data("imgwh").length > 0) {
            var wh = $(value).data("imgwh").split('*');
            postData.push("{ containerId: \"" + value + "\", width:" + wh[0] + ", height:" + wh[1] + " }");
        }
    });
    if (isReset) {
        $(classid).fileinput('destroy');
    }
    $(classid).fileinput({
        showUpload: false,
        showCaption: false,
        uploadAsync: true,
        dropZoneEnabled: false,
        showRemove: false,
        maxFileCount: 1,
        validateInitialCount: true,
        uploadUrl: "/tool/Upload/SingleMoreCutUpload",
        uploadExtraData: { imageSizes: "[" + postData + "]" },
        language: 'zh',
        allowedPreviewTypes: ['image'],
        allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg', 'bmp'],
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    }).on("filebatchselected", function (event, files) {
        $("div[id^='preview-'] button[class*='kv-file-zoom']").hide();
        if ($(arrayContainerId[0]).val().length == 0) {
            $(this).fileinput("upload");
        } else {
            $("div[id^='preview-'] button[class*='kv-file-upload']").hide();
            TipMsg("请先删除再上传!");
        }
    })
        .on("fileuploaded", function (event, data, previewId, index) {
            if (data.response.success) {
                $(data.response.data).each(function () {
                    $(this.containerId).val(this.filename);
                });
            } else {
                TipMsg(data.response.message);
                $.each(arrayContainerId, function (n, value) {
                    $(value).val("");
                });
            }
        }).on("fileremoved", function (event, id) {
            if (id.indexOf('preview') == -1) {
                $.each(arrayContainerId, function (n, value) {
                    $(value).val("");
                });
            }
        }).on("filesuccessremove", function (event, id) {
            $.each(arrayContainerId, function (n, value) {
                $(value).val("");
            });
        }).on("fileclear", function (event) {
            $.each(arrayContainerId, function (n, value) {
                $(value).val("");
            });
        });
}

function loadUploadify(fileid, containerId) {
    $(fileid).uploadify({
        swf: '/Areas/Admin/assets/js/uploadify/uploadify.swf',
        uploader: '/tool/Upload/SingleUpload?isFile=true&dir=Files',
        onSelect: function (sefile) {
            $(containerId).val("");
        }, onUploadComplete: function (sefile) {
            $(fileid).hide();
        },
        onUploadSuccess: function (sefile, data, response) {
            var result = JSON.parse(data);
            if (result.success) {
                $(containerId).val(result.data);
                if ($(fileid).parent().find("label").length > 0) {
                    $(fileid).parent().find("label").empty();
                    $(fileid).parent().find("label").remove();
                }
                $(containerId).parent().prepend("<label class=\"control-label no-padding-right\"><a href=\"" + imageServerUrl + result.data + "\">点击下载</a>&nbsp;&nbsp;<a onclick=\"uploadifyCancel('" + fileid + "','" + containerId + "')\" href=\"javascript:;\">重传</a></label>");
            } else {
                TipMsg(result.message, uploadifyshowBtn, fileid);
            }
        }
    });
}
function uploadifyshowBtn(fileid) {
    $(fileid).show();
}
function uploadifyCancel(fileid, containerId) {
    $(containerId).val("");
    $(fileid).show();
    if ($(fileid).parent().find("label").length > 0) {
        $(fileid).parent().find("label").empty();
        $(fileid).parent().find("label").remove();
    }
}
function savepwd() {
    if ($("#formpwd").valid()) {
        var type = $("#UserChangeType").val();
        $.ajax({
            type: "POST",
            url: "/Admin/Home/ChangeUserInfo", //用与处理ajax的地址及函数
            data: $("#formpwd").formSerialize(),
            dataType: "json",
            beforeSend: function () {
                $('#savepwd').off('click');
            },
            success: function (result) {
                if (result.success) {
                    $("#my-modal-user").modal("hide");
                    bootbox.alert(result.message, function () {
                        if (type == "1") {
                            location.href = "/Admin/Home/LoginOut";
                        }
                    });
                } else {
                    bootbox.alert(result.message);
                    $('#savepwd').on('click', savepwd);
                }
            },
            error: function (xhr, msg, e) {
                bootbox.alert("修改出错,请重试");
                $('#savepwd').on('click', savepwd);
            }
        });
    }
    return false;
}
$(function () {
    //校验修改密码表单
    $('#savepwd').on('click', savepwd);
    //以后可扩展
    var defmodal = ["my-modal-info", "my-modal-user"];
    var childId = "";
    var childindex = 0;
    $('body').on('hidden.bs.modal', '.modal', function () {
        if (defmodal.indexOf($(this).prop("id")) != -1) {
            if (childId != "" && childindex == 0) {
                childindex = 1;
                return;
            }
            $(this).removeData('bs.modal');
            $(this).find(".modal-body").empty();
            if ($(this).prop("id") == "my-modal-info") {
                //重置my-modal-info
                $("#my-modal-info .modal-content").html("<form class=\"form-horizontal\" enctype=\"multipart/form-data\" id=\"formDetail\" role=\"form\"><div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button><h3 class=\"smaller lighter blue no-margin\">详细信息</h3></div><div class=\"modal-body\"></div><div class=\"modal-footer\"><a class=\"btn btn-sm btn-success pull-right\" id=\"saveBtn\"><i class=\"ace-icon fa fa-check\"></i>保存</a></div></form>");
            }
            if (childindex == 1) {
                childId = "";
                childindex = 0;
            }
        } else {
            childId = $(this).prop("id");
            childindex = 0;
        }
    });
});
function showuser(type) {
    var url = "";
    var typename = "";
    switch (type) {
        case 'info':
            url = "/Admin/Home/ChangeLoginInfo?type=0";
            typename = "修改个人信息";
            break;
        case 'pwd':
            url = "/Admin/Home/ChangeLoginInfo?type=1";
            typename = "修改密码";
            break;
    }
    $.ajax({
        url: url,
        type: "get",
        success: function (data) {
            $("#my-modal-user h3").html(typename);
            $("#my-modal-user .modal-body").html(data);
            $("#my-modal-user").modal({ backdrop: 'static', keyboard: false });
        },
        error: function (e) {
            TipMsg("出错了,请稍后再试!");
        }
    });
}

function loadProvince(provinceId, cityId, districtId, isEmpty, provclassid, cityclassid, distclassid) {
    $.ajax({
        url: "/Admin/System/GetProvince?provinceId=" + provinceId + "&isEmpty=" + isEmpty,
        type: "get",
        dataType: "json",
        success: function (data) {
            var option = "";
            var index = 0;
            var cid = "";
            $(data).each(function (i, e) {
                option += "<option " + (e.selected == true ? "selected=\"selected\"" : "") + " value=\"" + e.value + "\">" + e.text + "</option>";
                if (index == 0) {
                    cid = e.value;
                }
                index++;
            });
            $(provclassid).html(option);
            $(provclassid).on("change", function () {
                var pid = $(this).val();
                loadCity({ provinceId: pid, cityId: "", districtId: "", isEmpty: isEmpty, cityclassid: cityclassid, distclassid: distclassid });
            });
            loadCity({ provinceId: (provinceId == "" || provinceId == "00000000-0000-0000-0000-000000000000" ? cid : provinceId), cityId: cityId, districtId: districtId, isEmpty: isEmpty, cityclassid: cityclassid, distclassid: distclassid });
        },
        error: function (e) {
            TipMsg("省份列表加载失败,请重试!");
        }
    });
}

function loadCity(parm) {
    $.ajax({
        url: "/Admin/System/GetCity?provinceId=" + parm.provinceId + "&cityId=" + parm.cityId + "&isEmpty=" + parm.isEmpty,
        type: "get",
        dataType: "json",
        success: function (data) {
            var option = "";
            var index = 0;
            var cid = "";
            $(data).each(function (i, e) {
                option += "<option " + (e.selected == true ? "selected=\"selected\"" : "") + " value=\"" + e.value + "\">" + e.text + "</option>";
                if (index == 0) {
                    cid = e.value;
                }
                index++;
            });
            $(parm.cityclassid).html(option);
            $(parm.cityclassid).on("change", function () {
                var cid = $(this).val();
                loadDistrict({ cityId: cid, districtId: "", isEmpty: parm.isEmpty, distclassid: parm.distclassid });
            });
            loadDistrict(parm.cityId == "" || parm.cityId == "00000000-0000-0000-0000-000000000000" ? { cityId: cid, districtId: parm.districtId, isEmpty: parm.isEmpty, distclassid: parm.distclassid } : parm);
        },
        error: function (e) {
            TipMsg("城市列表加载失败,请重试!");
        }
    });
}
function loadDistrict(parm) {
    $.ajax({
        url: "/Admin/System/GetDistrict?cityId=" + parm.cityId + "&districtId=" + parm.districtId + "&isEmpty=" + parm.isEmpty,
        type: "get",
        dataType: "json",
        success: function (data) {
            var option = "";
            $(data).each(function (i, e) {
                option += "<option " + (e.selected == true ? "selected=\"selected\"" : "") + " value=\"" + e.value + "\">" + e.text + "</option>";
            });
            $(parm.distclassid).html(option);
        },
        error: function (e) {
            TipMsg("地区列表加载失败,请重试!");
        }
    });
}
function GetLoginUser(callback) {
    $.ajax({
        url: "/Admin/Home/GetLoginByAjax",
        type: "get",
        dataType: "json",
        success: function (result) {
            if (result.success) {
                if (callback && result.data)
                    $.Callbacks().add(callback).fire(result.data);
                else if (callback)
                    $.Callbacks().add(callback).fire();
            }
        },
        error: function (e) {
            TipMsg("地区列表加载失败,请重试!");
        }
    });
    
}