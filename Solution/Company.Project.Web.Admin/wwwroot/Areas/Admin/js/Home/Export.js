﻿$(function () {
    $("#saveBtn").click(function () {
        var chkcanExport = false;
        $(".exportTitles").each(function () {
            if ($(this).prop("checked")) {
                chkcanExport = true;
            }
        });
        if (chkcanExport) {
            $("#exportForm").submit();
        } else {
            TipMsg("请至少选择一个列!");
        }
    });
});