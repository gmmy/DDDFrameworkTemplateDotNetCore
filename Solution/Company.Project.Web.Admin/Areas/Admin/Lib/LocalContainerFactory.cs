﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Project.Web.Admin.Areas.Admin.Lib.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Z.Nlayer.Domain.Base;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Web.Admin.Areas.Admin.Lib
{
    public static class LocalContainerFactory
    {
        public static void RegLocalServices(this IServiceCollection services)
        {
            services.AddTransient<ILocalCommon, LocalCommon>()
                .AddTransient<IAuthorizationHandler, AdminRoleHandler>();
        }
    }
}
