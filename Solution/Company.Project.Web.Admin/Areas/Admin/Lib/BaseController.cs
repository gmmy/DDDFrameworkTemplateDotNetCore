﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model.DTO.Manager;
using Company.Project.Web.Admin.Areas.Admin.Lib;
using Z.Nlayer.Utility;
using Z.Nlayer.Utility.Encrypt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using Microsoft.AspNetCore.Server.Kestrel.Internal.System.Text.Encodings.Web.Utf8;
using Newtonsoft.Json;
using Npoi.Core.SS.Formula.Functions;
using Company.Project.Infrastructure.Common;

namespace Company.Project.Web.Admin.Lib
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class BaseController : Controller
    {
        private readonly ILocalCommon _localCommon;

        public BaseController(ILocalCommon localCommon)
        {
           _localCommon = localCommon;
        }

        public dynamic GetRowToGrid<T>(EntityList<T> data) where T : new()
        {
            return NpoiHelper.GetRowToGrid(data);
        }

        public byte[] ReturnFile<T>(string excelName, List<string> titles, List<T> sOutput)
        {
            return new NpoiHelper(Response).ReturnFile(HttpUtility.UrlEncode(excelName,Encoding.UTF8), titles, sOutput);
        }

        public LoginUserDTO CurrentUser => _localCommon.CurrentFullUser();

        public string GetColumsName<T>(params JqGridOpt[] opt)
        {
            return _localCommon.GetColumsName<T>(opt);
        }
    }
}