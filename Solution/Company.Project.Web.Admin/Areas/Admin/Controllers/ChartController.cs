﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Company.Project.Application.Service.Interface;
using Company.Project.Web.Admin.Areas.Admin.Lib;
using Company.Project.Web.Admin.Lib;
using Company.Project.Web.Areas.Admin.Attrbute;
using Microsoft.AspNetCore.Mvc;

namespace Company.Project.Web.Areas.Admin.Controllers
{
    public class ChartController : BaseController
    {
        // GET: Admin/Chart
        private readonly IProject_Manager_LogServices _logServices;
        public ChartController(
            IProject_Manager_LogServices logServices, ILocalCommon localCommon
            ):base(localCommon)
        {
            _logServices = logServices;
        }
        #region 系统日志统计
        public IActionResult ManagerLogChart()
        {
            dynamic obj = new ExpandoObject();
            obj.Url = "ManagerLogCount";
            obj.Title = "系统日志统计";
            return View("~/Areas/Admin/Views/Chart/Chart.cshtml", obj);
        }
        [SubAuth(nameof(ManagerLogChart))]
        public async Task<IActionResult> ManagerLogCount(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            var result = await _logServices.ManagerLogChart(searchStartCreateTime, searchEndCreateTime);
            return Json(result);
        }

        #endregion

    }
}