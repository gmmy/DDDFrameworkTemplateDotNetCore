﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;
using Microsoft.EntityFrameworkCore;
using Z.Nlayer.Utility;

namespace Company.Project.Infrastructure.Repository.Sqlserver.EFWork
{
    public class Entities : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbType = Convert.ToInt32(ReadConfig.ReadAppSetting("dbType"));
            switch (dbType)
            {
                case 0:
                    optionsBuilder.UseSqlServer(ReadConfig.ReadAppSetting("sqlserver"));
                    break;
                case 1:
                    optionsBuilder.UseMySql(ReadConfig.ReadAppSetting("mysql"));
                    break;
            }

        }
        #region 实体引用

        public DbSet<Project_Manager_BaseInfo> Project_Manager_BaseInfo { get; set; }
        public DbSet<Project_SYS_Module> Project_SYS_Module { get; set; }
        public DbSet<Project_SYS_ModuleAction> Project_SYS_ModuleAction { get; set; }
        public DbSet<Project_SYS_Role> Project_SYS_Role { get; set; }
        public DbSet<Project_SYS_RoleAction> Project_SYS_RoleAction { get; set; }
        public DbSet<Project_Manager_Role> Project_Manager_Role { get; set; }
        public DbSet<Project_Manager_Log> Project_Manager_Log { get; set; }
        public DbSet<Project_SYS_WeChatSetting> Project_SYS_WeChatSetting { get; set; }
        public DbSet<Project_SYS_City> Project_SYS_City { get; set; }
        public DbSet<Project_SYS_District> Project_SYS_District { get; set; }
        public DbSet<Project_SYS_Province> Project_SYS_Province { get; set; }
        public DbSet<Project_SYS_PhoneMessageRecord> Project_SYS_PhoneMessageRecord { get; set; }
         
        public DbSet<RecursiveModuleDTO> RecursiveModuleDTO { get; set; }
        #endregion


        public virtual async Task CommitAsync()
        {
            await SaveChangesAsync();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            #region old code

            //指定实体字段保留小数位数
            //modelBuilder.Entity<Experience>().Property(a => a.dlongitude).HasPrecision(18, 6);
            //modelBuilder.Entity<Experience>().Property(a => a.dlatitude).HasPrecision(18, 6);

            //解除实体与数据库之间的映射
            modelBuilder.Entity<Project_SYS_Province>().Ignore(p => p.PyProvinceName);
            modelBuilder.Entity<Project_SYS_City>().Ignore(p => p.PyCityName);

            //系统管理员与管理员角色的多对多关联 主外键关联请手动按需添加
            //modelBuilder.Entity<Project_SYS_ModuleAction>().HasOne(a => a.Module).WithMany(c => c.ModuleActions).HasForeignKey(c => c.ModuleID);  //1:N
            //modelBuilder.Entity<Project_SYS_ModuleAction>().HasOne(o => o.Category).WithOne(o => o.Product);//1:1
            
            modelBuilder.Entity<Project_Manager_Role>().HasOne(a => a.BaseInfo).WithMany(a => a.Roles).HasForeignKey(a=>a.MemberId); //1:N
            modelBuilder.Entity<Project_SYS_RoleAction>().HasOne(a => a.Role).WithMany(a => a.RoleActions).HasForeignKey(a => a.RoleID);//1:N
            modelBuilder.Entity<Project_SYS_ModuleAction>().HasOne(a => a.Module).WithMany(c => c.ModuleActions).HasForeignKey(c => c.ModuleID); //1:N
            modelBuilder.Entity<Project_Manager_Log>().HasOne(a => a.UserBaseInfo).WithMany(c => c.Logs).HasForeignKey(c => c.UId);  //1:N
            #endregion
        }

        //方便排查异常信息
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var result = await base.SaveChangesAsync(cancellationToken);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
