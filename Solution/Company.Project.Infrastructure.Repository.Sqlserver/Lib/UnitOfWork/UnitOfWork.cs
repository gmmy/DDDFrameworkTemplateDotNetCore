﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Infrastructure.Repository.Sqlserver.EFWork;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository
{
    public class UnitOfWork : IUnitOfWorkFramework
    {
        private readonly IDatabaseFactory databaseFactory;
        private Entities dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        public Entities DataContext
        {
            get { return dataContext ?? (dataContext = databaseFactory.Get()); }
        }

        public async Task CommitAsync()
        {
            try
            {
                await DataContext.CommitAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.GetBaseException().Message);
            }
        }
    }
}
