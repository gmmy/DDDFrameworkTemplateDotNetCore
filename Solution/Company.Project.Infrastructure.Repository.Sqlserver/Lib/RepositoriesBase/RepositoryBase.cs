﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.Project.Infrastructure.Repository.Sqlserver.EFWork;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Z.Nlayer.Domain.Base;

namespace Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase
{
    public abstract class RepositoryBase<T> : RepositoryBaseFramework<T, Entities>
        where T : EntityBase, new()
    {

        protected RepositoryBase(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }


    }
}
