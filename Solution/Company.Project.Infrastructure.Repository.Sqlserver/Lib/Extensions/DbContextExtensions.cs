﻿using Z.Nlayer.Domain.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository.Sqlserver.Extensions
{
    public static class DbContextExtensions
    {
        public static void AttachUpdated<T>(this DbContext obj, T objectDetached) where T : EntityBase
        {
            if (objectDetached == null) throw new ArgumentNullException();
            if (obj.Entry<T>(objectDetached).State == EntityState.Detached)
            {
                obj.Set<T>().Attach(objectDetached);
            }
        }
    }
}
