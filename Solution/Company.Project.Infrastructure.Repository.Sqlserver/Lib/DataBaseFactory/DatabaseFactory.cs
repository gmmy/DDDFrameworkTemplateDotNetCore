﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.Project.Infrastructure.Repository.Sqlserver.EFWork;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private Entities dataContext;
        public Entities Get()
        {
            return dataContext ?? (dataContext = new Entities());
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (dataContext != null)
                        dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
