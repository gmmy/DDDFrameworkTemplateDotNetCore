﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    public interface IDatabaseFrameworkFactory<out T> : IDisposable
        where T : DbContext, new()
    {
        T Get();
    }
}
