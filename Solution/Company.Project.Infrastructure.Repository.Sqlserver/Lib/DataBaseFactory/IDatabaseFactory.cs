﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Z.Nlayer.Domain.Base;
using Company.Project.Infrastructure.Repository.Sqlserver.EFWork;

namespace Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    public interface IDatabaseFactory : IDatabaseFrameworkFactory<Entities>, IDependencyDynamicService
    {

    }
}
