﻿using System.Collections.Generic;
using System.Linq;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase;

namespace Company.Project.Infrastructure.Repository.Sqlserver
{
    public class Project_SYS_WeChatSettingRepository : RepositoryBase<Project_SYS_WeChatSetting>, IProject_SYS_WeChatSettingRepository
    {
        public Project_SYS_WeChatSettingRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}