﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository.Sqlserver
{
    public class Project_SYS_ModuleActionRepository : RepositoryBase<Project_SYS_ModuleAction>, IProject_SYS_ModuleActionRepository
    {
        public Project_SYS_ModuleActionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
        public async Task<List<LoginModultActionDTO>> GetModultAction()
        {
            var query = await (from a in dbset.Where(x => !x.IsDeleted)
                               join b in DataContext.Project_SYS_Module.Where(x => !x.IsDeleted) on a.ModuleID equals b.ID
                               join c1 in DataContext.Project_SYS_Module.DefaultIfEmpty() on b.PID equals c1.ID into tmp1
                               from c in tmp1.DefaultIfEmpty()
                               select new LoginModultActionDTO
                               {
                                   ActionUrl = a.ActionUrl,
                                   ShowEnum = a.ShowEnum,
                                   ActionCls = a.ActionCls,
                                   ActionFun = a.ActionFun,
                                   OrderID = a.OrderID,
                                   ActionName = a.ActionName,
                                   FatherModuleName = c == null ? "" : c.ModuleName,
                                   ModuleName = b.ModuleName,
                                   ModuleID = a.ModuleID,
                                   Weight = a.Weight
                               }).ToListAsync();
            return query;
        }
    }
}