﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Company.Project.Domain.Model.DTO.Manager;
using Microsoft.EntityFrameworkCore;

namespace Company.Project.Infrastructure.Repository.Sqlserver
{
    public class Project_SYS_RoleRepository : RepositoryBase<Project_SYS_Role>, IProject_SYS_RoleRepository
    {
        public Project_SYS_RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
        public async Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, ISpecification<Project_SYS_Role> specification,
            Dictionary<string, string> sort)
        {
            var query = (from a in dbset.Where(specification.SatisfiedBy())
                         select new RoleDTO()
                         {
                             ID = a.ID,
                             RoleName = a.RoleName,
                             Memo = a.Memo,
                             OrderID = a.OrderID
                         });
            return await FindAllAsyncAsQuery<RoleDTO>(DataSort(query, sort), page, rows);
        }

        public async Task<List<RoleListDTO>> LoadMangerRoleList(Guid id)
        {
            if (id == Guid.Empty)//Add
            {
                var allroes = (from roles in DataContext.Project_SYS_Role
                    orderby roles.OrderID ascending
                    where !roles.IsDeleted
                    select new RoleListDTO()
                    {

                        ID = roles.ID,
                        RoleName = roles.RoleName,
                        Ischeck = false

                    });
                return (await allroes.ToListAsync());
            }
            else//Editor
            {
                var Roles =await (from mamager in DataContext.Project_Manager_BaseInfo.Include(x => x.Roles).ThenInclude(x => x.BaseInfo).Where(x => x.ID == id) where !mamager.IsDeleted select mamager.Roles.Where(x=>!x.IsDeleted).Select(x => x.RoleId)).ToListAsync();
                var chkRole = Roles.Aggregate((a, b) => a.Concat(b));
                var allrole = (from role in dbset.Where(x => !x.IsDeleted)
                               select new RoleListDTO()
                               {
                                   ID = role.ID,
                                   RoleName = role.RoleName,
                                   Ischeck = chkRole.Count(x => x == role.ID) > 0
                               }).ToList();
                return allrole;
            }
        }

        public async Task<Project_SYS_Role> GetRoleAndAction(Guid id)
        {
            return await (from a in dbset.Where(x => !x.IsDeleted && x.ID == id).Include(x => x.RoleActions)
                .ThenInclude(x => x.Role) select a).FirstOrDefaultAsync();
        }
    }
}