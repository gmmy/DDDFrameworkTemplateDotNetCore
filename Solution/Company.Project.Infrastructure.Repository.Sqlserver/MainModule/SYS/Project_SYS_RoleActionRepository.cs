﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase;

namespace Company.Project.Infrastructure.Repository.Sqlserver
{
    public class Project_SYS_RoleActionRepository : RepositoryBase<Project_SYS_RoleAction>, IProject_SYS_RoleActionRepository
    {
        public Project_SYS_RoleActionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}