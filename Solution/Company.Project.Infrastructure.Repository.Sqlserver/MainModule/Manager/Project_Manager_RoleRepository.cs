﻿using System;
using System.Collections.Generic;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Company.Project.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using System.Linq;

namespace Company.Project.Infrastructure.Repository.Sqlserver
{
    public class Project_Manager_RoleRepository : RepositoryBase<Project_Manager_Role>, IProject_Manager_RoleRepository
    {
        public Project_Manager_RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

       
    }
}