﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service.CacheService
{
    public interface IProject_SYS_BaseCacheService : IBaseCacheService, IDependencyDynamicService
    {
        void CacheLogin(LoginUserDTO dto);

        void RemoveLogin(IEnumerable<Guid> deld);

        List<LoginUserDTO> GetAllLogin();

        Task UpdateLogin(LoginUserDTO dto, int type);
    }
}
