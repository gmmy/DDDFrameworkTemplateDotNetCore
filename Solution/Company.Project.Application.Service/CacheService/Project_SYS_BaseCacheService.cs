﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model.DTO.Manager;
using Z.Nlayer.Infrastructure.Cache;
using Z.Nlayer.Utility;
using Z.Nlayer.Utility.Encrypt;

namespace Company.Project.Application.Service.CacheService
{
    public class Project_SYS_BaseCacheService: BaseCacheService, IProject_SYS_BaseCacheService
    {
        private readonly IProject_Manager_BaseInfoDomainServices _baseInfoDomainServices;
        private readonly IProject_Manager_BaseInfoRepository _baseInfoRepository;

        public Project_SYS_BaseCacheService(ICachePolicy cachePolicy, IProject_Manager_BaseInfoDomainServices baseInfoDomainServices
            , IProject_Manager_BaseInfoRepository baseInfoRepository):base(cachePolicy)
        {
            //TODO:可以设置一些Key命名规则 等等
            _baseInfoDomainServices = baseInfoDomainServices;
            _baseInfoRepository = baseInfoRepository;
        }
        #region 登录缓存相关

        public void CacheLogin(LoginUserDTO dto)
        {
            var loginUser = GetCache<List<LoginUserDTO>>("LoginUser");
            if (loginUser == null)
            {
                Add("LoginUser", new List<LoginUserDTO>() { dto }, DateTime.Now.AddYears(1));
            }
            else
            {
                var old = loginUser.FirstOrDefault(x => x.ID == dto.ID);
                if (old != null)
                {
                    loginUser.Remove(old);
                }
                loginUser.Add(dto);
                Remove("LoginUser");
                Add("LoginUser", loginUser, DateTime.Now.AddYears(1));
            }
        }

        public void RemoveLogin(IEnumerable<Guid> deld)
        {
            var loginUser = GetCache<List<LoginUserDTO>>("LoginUser");
            if (loginUser != null)
            {
                var old = loginUser.Where(x => deld.Contains(x.ID)).ToList();
                old.ForEach(x =>
                {
                    loginUser.Remove(x);
                });
                Remove("LoginUser");
                Add("LoginUser", loginUser, DateTime.Now.AddYears(1));
            }
        }

        public List<LoginUserDTO> GetAllLogin()
        {
            return GetCache<List<LoginUserDTO>>("LoginUser") ?? new List<LoginUserDTO>();
        }

        public async Task UpdateLogin(LoginUserDTO dto, int type)
        {
            if (dto.ID != Guid.Empty)
            {
                var loginUser = GetCache<List<LoginUserDTO>>("LoginUser");
                if (loginUser != null)
                {
                    var old = loginUser.FirstOrDefault(x => x.ID == dto.ID);
                    if (old != null)
                    {
                        if (!string.IsNullOrEmpty(dto.NickName))
                        {
                            old.NickName = dto.NickName;
                        }
                        switch (type)
                        {
                            case 1:
                                if (!string.IsNullOrEmpty(dto.PassWord))
                                {
                                    old.PassWord = MD5Helper.GetMd5(dto.PassWord);
                                }
                                break;
                            case 2:
                                if (!string.IsNullOrEmpty(dto.PassWord))
                                {
                                    old.PassWord = dto.PassWord;
                                }
                                old.State = dto.State;
                                var item = await _baseInfoRepository.GetByKeyAsync(old.ID);
                                old = await _baseInfoDomainServices.BindDTO(item);
                                break;
                        }
                    }
                    Remove("LoginUser");
                    Add("LoginUser", loginUser, DateTime.Now.AddYears(1));
                }
            }
        }

        #endregion
    }
}
