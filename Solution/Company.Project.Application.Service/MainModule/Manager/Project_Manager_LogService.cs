﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service
{
    public class Project_Manager_LogServices : BaseServices<Project_Manager_Log>, IProject_Manager_LogServices
    {
        private readonly IProject_Manager_LogDomainServices _domainServices;
        private readonly IProject_Manager_LogRepository _repository;
        public Project_Manager_LogServices(IUnitOfWorkFramework unitOfWork, IProject_Manager_LogRepository repository, IProject_Manager_LogDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }
		public async Task<EntityList<LogDTO>> GetLogList(int page, int rows, Dictionary<string, string> sort, DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText)
        {
            return await _repository.GetLogList(page, rows, new LogListSpecification(searchStartCreateTime, searchEndCreateTime, searchText), sort);
        }

        public async Task LogAsync(string title, string Contents, bool isBulk = false, LoginUserDTO mydto = null)
        {
            _domainServices.Log(title, Contents, mydto);
            await unitOfWork.CommitAsync();
        }
        public async Task<List<dynamic>> ManagerLogChart(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            return await _repository.ManagerLogChart(new ManagerLogChartSpecification(searchStartCreateTime, searchEndCreateTime));
        }
    }
}