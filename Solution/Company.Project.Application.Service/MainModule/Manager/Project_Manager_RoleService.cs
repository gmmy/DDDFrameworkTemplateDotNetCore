﻿using System;
using System.Collections.Generic;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Application.Service
{
    public class Project_Manager_RoleServices : BaseServices<Project_Manager_Role>, IProject_Manager_RoleServices
    {
        private readonly IProject_Manager_RoleDomainServices _domainServices;
        private readonly IProject_Manager_RoleRepository _repository;
        public Project_Manager_RoleServices(IUnitOfWorkFramework unitOfWork, IProject_Manager_RoleRepository repository, IProject_Manager_RoleDomainServices domainService)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
        }

    }
}