﻿using System.Collections.Generic;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service
{
    public class Project_SYS_DistrictServices : BaseServices<Project_SYS_District>, IProject_SYS_DistrictServices
    {
        private readonly IProject_SYS_DistrictDomainServices _domainServices;
        private readonly IProject_SYS_DistrictRepository _repository;
        //private readonly IProject_Manager_LogServices _logServices;
        public Project_SYS_DistrictServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_DistrictRepository repository, IProject_SYS_DistrictDomainServices domainService
		//,IProject_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			//_logServices = logServices;
        }
    }
}