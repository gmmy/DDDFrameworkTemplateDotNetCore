﻿using System.Collections.Generic;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service
{
    public class Project_SYS_CityServices : BaseServices<Project_SYS_City>, IProject_SYS_CityServices
    {
        private readonly IProject_SYS_CityDomainServices _domainServices;
        private readonly IProject_SYS_CityRepository _repository;
        //private readonly IProject_Manager_LogServices _logServices;
        public Project_SYS_CityServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_CityRepository repository, IProject_SYS_CityDomainServices domainService
		//,IProject_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			//_logServices = logServices;
        }
    }
}