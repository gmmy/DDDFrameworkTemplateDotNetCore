﻿using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Application.Service
{
    public class Project_SYS_ModuleActionServices : BaseServices<Project_SYS_ModuleAction>, IProject_SYS_ModuleActionServices
    {
        private readonly IProject_SYS_ModuleActionDomainServices _domainServices;
        private readonly IProject_SYS_ModuleActionRepository _repository;
        public Project_SYS_ModuleActionServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_ModuleActionRepository repository, IProject_SYS_ModuleActionDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

    }
}