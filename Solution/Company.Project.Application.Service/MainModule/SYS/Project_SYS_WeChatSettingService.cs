﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service
{
    public class Project_SYS_WeChatSettingServices : BaseServices<Project_SYS_WeChatSetting>, IProject_SYS_WeChatSettingServices
    {
        private readonly IProject_SYS_WeChatSettingDomainServices _domainServices;
        private readonly IProject_SYS_WeChatSettingRepository _repository;
        private readonly IProject_Manager_LogServices _logServices;
        public Project_SYS_WeChatSettingServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_WeChatSettingRepository repository, IProject_SYS_WeChatSettingDomainServices domainService
            ,IProject_Manager_LogServices logServices
            )
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        public async Task ModifyModel(Project_SYS_WeChatSetting item)
        {
            _domainServices.ModifyModel(item);
            await _logServices.LogAsync("微信公众号设置", $"公众号名称=[{item.WeChatName}],公众号原始ID=[{item.WxChat_gID}],微信Token=[{item.Token}],微信绑定Url=[{item.BindUrl}],AppID=[{item.AppID}],AppSecret=[{item.AppSecret}]");
        }
    }
}