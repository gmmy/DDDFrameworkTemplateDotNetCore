﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.Nlayer.Utility;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.CacheService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service
{
    public class Project_SYS_ProvinceServices : BaseServices<Project_SYS_Province>, IProject_SYS_ProvinceServices
    {
        private readonly IProject_SYS_ProvinceDomainServices _domainServices;
        private readonly IProject_SYS_ProvinceRepository _repository;
        private readonly IProject_SYS_BaseCacheService _baseCacheService;
        private readonly IProject_SYS_CityRepository _cityRepository;
        public Project_SYS_ProvinceServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_ProvinceRepository repository, IProject_SYS_ProvinceDomainServices domainService,
            IProject_SYS_BaseCacheService baseCacheService,
            IProject_SYS_CityRepository cityRepository
        //,IProject_Manager_LogServices logServices
        )
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
            _baseCacheService = baseCacheService;
            _cityRepository = cityRepository;
        }

        /// <summary>
        /// 从数据库加载省市列表存入缓存
        /// </summary>
        /// <returns></returns>
        public async Task<Tuple<List<Project_SYS_Province>, List<Project_SYS_City>>> LoadProvinceandCity()
        {
            List<Project_SYS_Province> pubPorv;
            List<Project_SYS_City> pubCity;
            var wxProvince = _baseCacheService.GetCache<List<Project_SYS_Province>>("WxProvince");
            if (wxProvince == null)
            {
                var myprov = await _repository.GetAllAsync();
                var prov = myprov.ToList();
                prov.ForEach(x =>
                {
                    x.PyProvinceName =
                        x.ProvinceName.ToCharArray()
                            .ToList()
                            .ConvertAll(y => y.ToString())
                            .Aggregate((a, b) => PinYinHelper.Get(a) + PinYinHelper.Get(b));
                });
                _baseCacheService.Add("WxProvince", prov);
                pubPorv = prov.ToList();
            }
            else
            {
                pubPorv = wxProvince;
            }
            var wxCity = _baseCacheService.GetCache<List<Project_SYS_City>>("WxCity");
            if (wxCity == null)
            {
                var mycity = await _cityRepository.GetAllAsync();
                var city = mycity.ToList();
                city.ForEach(x =>
                {
                    x.PyCityName =
                        x.CityName.ToCharArray()
                            .ToList()
                            .ConvertAll(y => y.ToString())
                            .Aggregate((a, b) => PinYinHelper.Get(a) + PinYinHelper.Get(b));
                });
                _baseCacheService.Add("WxCity", city);
                pubCity = city;
            }
            else
            {
                pubCity = wxCity;
            }
            return new Tuple<List<Project_SYS_Province>, List<Project_SYS_City>>(pubPorv, pubCity);
        }
    }
}