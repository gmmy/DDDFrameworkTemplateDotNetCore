﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Utility;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Infrastructure.Common;
using Company.Project.Infrastructure.Common.Enums;

namespace Company.Project.Application.Service
{
    public class Project_SYS_PhoneMessageRecordServices : BaseServices<Project_SYS_PhoneMessageRecord>, IProject_SYS_PhoneMessageRecordServices
    {
        private readonly IProject_SYS_PhoneMessageRecordDomainServices _domainServices;
        private readonly IProject_SYS_PhoneMessageRecordRepository _repository;
        public Project_SYS_PhoneMessageRecordServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_PhoneMessageRecordRepository repository, IProject_SYS_PhoneMessageRecordDomainServices domainService

		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

        public async Task SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType, string sendOpenId, Guid? sendUserId)
        {
            try
            {
                var contents = await _domainServices.SendSingleMessage(telPhone, sendType, Enum_PhoneMessageSendUserType.WeChatMember, sendOpenId,
                    IPHelper.getIPAddr(), sendUserId);
                await unitOfWork.CommitAsync();
                //TODO:接入短信接口

            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception e)
            {
                LoggerHelper.Log("短信发送异常:" + e.Message + ",详细错误:" + e.GetBaseException().Message);
                throw new CustomException("短信发送失败,请稍后重试!");
            }
        }
    }
}