﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service
{
    public class Project_SYS_ModuleServices : BaseServices<Project_SYS_Module>, IProject_SYS_ModuleServices
    {
        private readonly IProject_SYS_ModuleDomainServices _domainServices;
        private readonly IProject_SYS_ModuleRepository _repository;
        private readonly IProject_Manager_LogServices _logServices;
        public Project_SYS_ModuleServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_ModuleRepository repository, IProject_SYS_ModuleDomainServices domainService,
            IProject_Manager_LogServices logServices)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        public async Task AddModule(Project_SYS_Module item)
        {
            var ID = item.ID;
            await _domainServices.AddModule(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "模块", $"模块名称=[{item.ModuleName}],模块地址=[{item.ModuleUrl}],排序编号=[{item.OrderID}],=[{item.ModuleCls}]");
        }

        public async Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId)
        {
            return await _repository.GetFatherModule(fatherId, itemId);
        }

        public async Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId)
        {
            return await _repository.LoadModuleList(RoleId);
        }


        public async Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows,Guid pid, Dictionary<string, string> sort)
        {
            return await _repository.GetModuleList(page, rows, new ModuleListSpecification(pid), sort);
        }

        public async Task DelModule(IEnumerable<Guid> delId)
        {
            var delName =
                (await _domainServices.DelModule(delId));
            await _logServices.LogAsync("删除模块", "模块名称:[" + string.Join(",", delName) + "]");
        }
    }
}