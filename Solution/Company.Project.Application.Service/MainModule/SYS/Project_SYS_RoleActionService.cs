﻿using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Application.Service
{
    public class Project_SYS_RoleActionServices : BaseServices<Project_SYS_RoleAction>, IProject_SYS_RoleActionServices
    {
        private readonly IProject_SYS_RoleActionDomainServices _domainServices;
        private readonly IProject_SYS_RoleActionRepository _repository;
        public Project_SYS_RoleActionServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_RoleActionRepository repository, IProject_SYS_RoleActionDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

    }
}