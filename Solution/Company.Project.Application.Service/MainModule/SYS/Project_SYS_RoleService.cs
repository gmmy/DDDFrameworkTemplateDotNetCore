﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Company.Project.Application.Service.Interface;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.MainModule.Specification;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service
{
    public class Project_SYS_RoleServices : BaseServices<Project_SYS_Role>, IProject_SYS_RoleServices
    {
        private readonly IProject_SYS_RoleDomainServices _domainServices;
        private readonly IProject_SYS_RoleRepository _repository;
        private readonly IProject_Manager_LogServices _logServices;
        public Project_SYS_RoleServices(IUnitOfWorkFramework unitOfWork, IProject_SYS_RoleRepository repository, IProject_SYS_RoleDomainServices domainService,
            IProject_Manager_LogServices logServices)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }
        public async Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, Dictionary<string, string> sort)
        {
            return await _repository.GetRoleList(page, rows, new RoleListSpecification(), sort);
        }

        public async Task AddRole(Project_SYS_Role model, List<Project_SYS_ModuleAction> moduleAction)
        {
            var ID = model.ID;
            await _domainServices.AddRole(model, moduleAction);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "角色", $"角色名称{model.RoleName},排序编号{model.OrderID},角色描述{model.Memo}");
        }

        public async Task <List<RoleListDTO>> LoadMangerRoleList(Guid id)
        {
            return await _repository.LoadMangerRoleList(id);
        }

        public async Task DelRole(IEnumerable<Guid> ids)
        {
            var delName = await _domainServices.DelRole(ids);
            await _logServices.LogAsync("删除角色", $"角色名称{string.Join(",", delName)}");
        }
    }
}