﻿using System;
using Z.Nlayer.Application.BaseService;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Z.Nlayer.Domain.Base;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Company.Project.Infrastructure.Common.Enums;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_Manager_BaseInfoServices : IBaseServices<Project_Manager_BaseInfo>, IDependencyDynamicService
    {
        Task CheckLogin(string loginName,string passWord,bool remmberMe, bool? pwdIsMd5 = false, string validateCode = null);
        Task ChangeUserInfo(Guid ID, string nickName, string newPassword, string confirmPassword,int UserChangeType, string oldpassword);
        Task<EntityList<BaseInfoDTO>> GetManagerList(int page, int rows, Dictionary<string, string> sort, Enum_ManagerUserState? State,Guid? Role, string sealogin);
        Task AddMangerBaseInfo(Project_Manager_BaseInfo model,Guid id) ;

        Task DelManager(IEnumerable<Guid> deld , Guid uid);
    }
}