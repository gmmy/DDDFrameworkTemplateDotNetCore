﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_Manager_LogServices : IBaseServices<Project_Manager_Log>, IDependencyDynamicService
    {
		Task<EntityList<LogDTO>> GetLogList(int page, int rows, Dictionary<string, string> sort, DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText);
        Task LogAsync(string title, string Contents, bool isBulk = false, LoginUserDTO mydto = null);
        Task<List<dynamic>> ManagerLogChart(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime);
    }
}