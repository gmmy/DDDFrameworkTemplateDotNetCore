﻿using System;
using Z.Nlayer.Application.BaseService;
using Company.Project.Domain.Model;
using System.Collections.Generic;
using Z.Nlayer.Domain.Base;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_Manager_RoleServices : IBaseServices<Project_Manager_Role>, IDependencyDynamicService
    {
       
    }
}