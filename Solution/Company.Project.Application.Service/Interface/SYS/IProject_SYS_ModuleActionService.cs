﻿using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_ModuleActionServices : IBaseServices<Project_SYS_ModuleAction>, IDependencyDynamicService
    {

    }
}