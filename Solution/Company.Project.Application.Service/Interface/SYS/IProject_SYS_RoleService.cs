﻿using System.Collections.Generic;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using System;
using System.Threading.Tasks;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_RoleServices : IBaseServices<Project_SYS_Role>, IDependencyDynamicService
    {
        Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, Dictionary<string, string> sort);
        Task AddRole(Project_SYS_Role model, List<Project_SYS_ModuleAction> moduleAction);
        Task<List<RoleListDTO>> LoadMangerRoleList(Guid id);
        Task DelRole(IEnumerable<Guid> ids);
    }
}