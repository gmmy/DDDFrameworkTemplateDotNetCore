﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Infrastructure.Common.Enums;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_PhoneMessageRecordServices : IBaseServices<Project_SYS_PhoneMessageRecord>, IDependencyDynamicService
    {
        Task SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType, string sendOpenId, Guid? sendUserId);
    }
}