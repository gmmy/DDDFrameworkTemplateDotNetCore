﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_ProvinceServices : IBaseServices<Project_SYS_Province>, IDependencyDynamicService
    {

        /// <summary>
        /// 从数据库加载省市列表存入缓存
        /// </summary>
        /// <returns></returns>
        Task<Tuple<List<Project_SYS_Province>, List<Project_SYS_City>>> LoadProvinceandCity();
    }
}