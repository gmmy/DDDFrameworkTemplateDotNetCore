﻿using System.Collections.Generic;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_CityServices : IBaseServices<Project_SYS_City>, IDependencyDynamicService
    {
    }
}