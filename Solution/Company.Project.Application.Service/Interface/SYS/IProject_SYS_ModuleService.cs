﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_ModuleServices : IBaseServices<Project_SYS_Module>, IDependencyDynamicService
    {
        Task AddModule(Project_SYS_Module item);
        Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId);
        Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId);
        Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows,Guid pid, Dictionary<string, string> sort);

        Task DelModule(IEnumerable<Guid> delId);
    }
}