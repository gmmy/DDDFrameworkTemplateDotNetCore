﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;

namespace Company.Project.Application.Service.Interface
{
    public interface IProject_SYS_WeChatSettingServices : IBaseServices<Project_SYS_WeChatSetting>, IDependencyDynamicService
    {
        Task ModifyModel(Project_SYS_WeChatSetting item);
    }
}