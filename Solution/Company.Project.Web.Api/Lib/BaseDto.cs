﻿using Company.Project.Infrastructure.Common.CustomerAttribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Company.Project.Web.Api.Lib
{
    public class BaseDto
    {
        [BaseCustom(DisplayName = "时间戳", MaxLen = 14, NullValue = true, NullField = true)]
        public string Timespan { get; set; }

        [BaseCustom(DisplayName = "签名",MaxLen = 32, NullValue = true, NullField = true)]
        public string Sign { get; set; }
    }
}
