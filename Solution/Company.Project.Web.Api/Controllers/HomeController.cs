﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Company.Project.Application.Service.Interface;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Web.Api.Dto;
using Company.Project.Web.Api.Lib;
using Z.Nlayer.Utility;
using Microsoft.AspNetCore.Mvc;
using Company.Project.Web.Api.Dto.Test;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Company.Project.Web.Api.Controllers
{
    [Route("api/Home")]
    public class HomeController : BaseApiController
    {
        private readonly IProject_Manager_BaseInfoServices _baseInfoServices;
        public HomeController(IProject_Manager_BaseInfoServices baseInfoServices)
        {
            _baseInfoServices = baseInfoServices;
        }
        // GET: api/values
        [HttpPost, Route("Get")]
        public async Task<dynamic> get()
        {
            return await Do<EmptyDto>(async (dto, result) =>
            {
                result.data = (await _baseInfoServices.GetAllAsync()).FirstOrDefault();
                result.message = "操作成功!";
            });
        }
        [HttpPost, Route("Post")]
        public async Task<APITipResult> Post()
        {
            return await Do<MyTestInputDto>(async (dto, result) =>
            {
                result.data = new MyTestOutputDto() {Name = "王军", AgeValue = 5};
                result.message = "操作成功!";
            });
        }
    }
}
