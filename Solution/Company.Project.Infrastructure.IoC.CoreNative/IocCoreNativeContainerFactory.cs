﻿using System;
using System.Collections.Generic;
using System.Text;
using Z.Nlayer.Application.BaseService;
using Z.Nlayer.Domain.Base;
using Z.Nlayer.Infrastructure.Cache;
using Company.Project.Infrastructure.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace Company.Project.Infrastructure.IoC.CoreNative
{
    public static class IocCoreNativeContainerFactory
    {
        public static void RegServices(this IServiceCollection services)
        {
            //对基础设施的依赖注入
           
            services
                .AddScoped<IUnitOfWorkFramework, UnitOfWork>()
                .AddTransient<ICachePolicy, HttpRunTimeCachePolicy>()
                .AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //对业务的依赖注入
            services.RegisterAssembly();
        }
    }
}
