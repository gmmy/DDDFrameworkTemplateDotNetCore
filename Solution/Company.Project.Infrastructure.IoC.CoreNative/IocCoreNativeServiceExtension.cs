﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;
using Z.Nlayer.Domain.Base;
using Z.Nlayer.Utility;

namespace Company.Project.Infrastructure.IoC.CoreNative
{
    /// <summary>
    /// 通过反射动态构建依赖注入
    /// </summary>
    public static class IocCoreNativeServiceExtension
    {
        public static IServiceCollection RegisterAssembly(this IServiceCollection service)
        {
            AssemblyHelper.GetAllAssemblies().ForEach(interfaceAssembly =>
            {
                var types = interfaceAssembly.GetTypes().Where(t =>
                    t.GetTypeInfo().IsInterface && !t.GetTypeInfo().IsGenericType &&
                    t.GetInterfaces().Contains(typeof(IDependencyDynamicService)));
                foreach (var type in types)
                {
                    var implementTypeName = type.Name.Substring(1);
                    var implementType = AssemblyHelper.GetImplementType(implementTypeName, type);
                    if (implementType != null)
                    {
                        service.AddScoped(type, implementType);

                    }
                    else
                    {
                        LoggerHelper.Log($"IOC error:{implementTypeName},type not found");
                    }
                }
            });
            return service;
        }
    }
}
