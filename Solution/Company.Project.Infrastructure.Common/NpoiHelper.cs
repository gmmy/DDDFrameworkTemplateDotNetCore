﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json.Linq;
using Npoi.Core.HSSF.UserModel;


namespace Company.Project.Infrastructure.Common
{
    public class NpoiHelper
    {
        private HttpResponse Response { get; set; }
        public NpoiHelper(HttpResponse response)
        {
            Response = response;
        }

        public static dynamic GetRowToGrid(dynamic data)
        {
            return new
            {
                page = data.Page,
                total = data.TotalPage,
                records = data.Total,
                rows = data.Data
            };
        }
        public byte[] ReturnFile<T>(string excelName, List<string> titles, List<T> sOutput)
        {
            int chkAttribute = 0;
            List<SelectListItem> titleFound = null;
            var workbook = new HSSFWorkbook();
            var maxIndex = 65535;//Excel单个sheet最大行数
            var sheetIndex = 1;
            for (var i = 0; i < ((sOutput.Count <= maxIndex ? 1 : sOutput.Count % maxIndex == 0 ? sOutput.Count / maxIndex : sOutput.Count / maxIndex + 1)); i++)
            {
                var result = sOutput.Skip(maxIndex * i).Take(maxIndex);
                AddRow<T>(workbook, sheetIndex, titles, result.ToList(), chkAttribute, titleFound);
                sheetIndex++;
            }
            Response.Headers.Append("Content-Disposition",
                "attachment;filename=\"" + excelName + ".xls\"");
            Response.Headers.Append("Content-Encoding",
                "GB2312");
            MemoryStream ms = new MemoryStream();
            workbook.Write(ms);
            return ms.ToArray();
        }

        public void AddRow<T>(HSSFWorkbook workbook, int sheetIndex, List<string> titles, List<T> sOutput, int chkAttribute = 0, List<SelectListItem> titleFound = null)
        {
            var sheet = workbook.CreateSheet("sheet" + sheetIndex);
            var headerRow = sheet.CreateRow(0);
            var index_x = 1;
            var index_y = 0;
            titles.ForEach(x =>
            {
                headerRow.CreateCell(index_y).SetCellValue(x.Split('|')[1]);
                index_y++;
            });
            //排除虚方法与没有DisplayName的字段
            if (chkAttribute == 0)
            {
                //纯泛型导出
                var showColumns =
                    typeof(T).GetProperties()
                        .Where(
                            x =>
                                !x.GetMethod.IsVirtual &&
                                titles.Select(y => y.Split('|')[0]).Contains("_" + x.Name))
                        .Select(x => x.Name).ToList();
                sOutput.ForEach(x =>
                {
                    index_y = 0;
                    var thisRow = sheet.CreateRow(index_x);
                    x.GetType()
                        .GetProperties()
                        .ToList()
                        .Where(y => !y.GetMethod.IsVirtual && showColumns.Contains(y.Name))
                        .Select(y => y.GetValue(x))
                        .ToList()
                        .ForEach(y =>
                        {
                            var val = y;
                            if (val is DateTime)
                            {
                                if (Convert.ToDateTime(val) == DateTime.MinValue)
                                {
                                    val = "-";
                                }
                            }
                            var cell = thisRow.CreateCell(index_y);
                            cell.SetCellValue(val == null ? "-" : val.ToString());
                            index_y++;
                        });
                    index_x++;
                });
            }
            else if (chkAttribute == 1)
            {
                //纯DataTable导出
                //sOutput.ForEach(x =>
                //{
                //    index_y = 0;
                //    var thisRow = sheet.CreateRow(index_x);
                //    foreach (JProperty jp in JToken.Parse(x.ToString()))
                //    {
                //        var val = jp.Value;
                //        var cell = thisRow.CreateCell(index_y);
                //        cell.SetCellValue(val == null ? "-" : val.ToString());
                //        index_y++;
                //    }
                //    index_x++;
                //});
            }
            else if (chkAttribute == 2)
            {
                //混合式导出
                //sOutput.ForEach(x =>
                //{
                //    index_y = 0;
                //    var thisRow = sheet.CreateRow(index_x);
                //    titles.ForEach(y =>
                //    {
                //        var titleVal = titleFound.FirstOrDefault(z => z.Text == y);
                //        foreach (JProperty jp in JToken.Parse(x.ToString()))
                //        {
                //            if (titleVal.Value.Substring(1, titleVal.Value.Length - 1) == jp.Name.ToString())
                //            {
                //                var val = jp.Value;
                //                var cell = thisRow.CreateCell(index_y);
                //                cell.SetCellValue(val == null ? "-" : val.ToString());
                //                index_y++;
                //            }
                //        }
                //    });
                //    index_x++;
                //});
            }
        }
    }
}
