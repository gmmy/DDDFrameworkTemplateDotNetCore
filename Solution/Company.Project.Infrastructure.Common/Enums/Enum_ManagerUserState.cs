﻿using System.ComponentModel;
using Company.Project.Infrastructure.Common;

using Z.Nlayer.Utility;
namespace Company.Project.Infrastructure.Common.Enums
{
    public enum Enum_ManagerUserState
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 0,
        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Locked = 1
    }
}
