﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Company.Project.Infrastructure.Common
{
    public class JqGridOpt
    {
        public string Title { get; set; }
        public string OptMainFun { get; set; }
        public int OptWidth { get; set; }
    }
}
