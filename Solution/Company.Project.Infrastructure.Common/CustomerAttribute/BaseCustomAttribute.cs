﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Project.Infrastructure.Common.CustomerAttribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BaseCustomAttribute : Attribute
    {
        public string DisplayName { get; set; }
        public int MaxLen { get; set; }
        public int MinLen { get; set; } = 0;
        public bool NullValue { get; set; }
        public bool NullField { get; set; } = false;
    }
}
