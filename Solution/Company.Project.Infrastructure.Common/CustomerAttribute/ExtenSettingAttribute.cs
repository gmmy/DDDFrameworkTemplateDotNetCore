﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.Project.Infrastructure.Common.CustomerAttribute
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ExtenSettingAttribute : Attribute
    {
        public ExtenSettingAttribute(bool isExten)
        {
            Exten = isExten;
        }
        public bool Exten { get; set; }
    }
}
