﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using System;
namespace Company.Project.Domain.MainModule
{
    public class Project_SYS_DistrictDomainServices : IProject_SYS_DistrictDomainServices
    {
        private readonly IProject_SYS_DistrictRepository _repository;

        public Project_SYS_DistrictDomainServices(IProject_SYS_DistrictRepository repository)
        {
            this._repository = repository;
        }
        
    }
}
