﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule
{
    public class Project_SYS_ModuleActionDomainServices : IProject_SYS_ModuleActionDomainServices
    {
        private readonly IProject_SYS_ModuleActionRepository _repository;

        public Project_SYS_ModuleActionDomainServices(IProject_SYS_ModuleActionRepository repository)
        {
            this._repository = repository;
        }
    }
}
