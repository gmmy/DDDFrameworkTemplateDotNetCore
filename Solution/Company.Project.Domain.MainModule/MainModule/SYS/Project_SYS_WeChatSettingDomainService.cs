﻿using System;
using System.Threading.Tasks;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule
{
    public class Project_SYS_WeChatSettingDomainServices : IProject_SYS_WeChatSettingDomainServices
    {
        private readonly IProject_SYS_WeChatSettingRepository _repository;

        public Project_SYS_WeChatSettingDomainServices(IProject_SYS_WeChatSettingRepository repository)
        {
            this._repository = repository;
        }

        public void ModifyModel(Project_SYS_WeChatSetting item)
        {
            if (item.ID == Guid.Empty)
            {
                item.ID = Guid.NewGuid();
                item.CreateTime = DateTime.Now;
                _repository.Add(item);
            }
            else
            {
                _repository.Update(item, x => x.WeChatName, x => x.WxChat_gID, x => x.Token, x => x.BindUrl, x => x.AppID, x => x.AppSecret);
            }
        }
    }
}
