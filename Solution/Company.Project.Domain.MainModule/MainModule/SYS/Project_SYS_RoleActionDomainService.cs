﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule
{
    public class Project_SYS_RoleActionDomainServices : IProject_SYS_RoleActionDomainServices
    {
        private readonly IProject_SYS_RoleActionRepository _repository;

        public Project_SYS_RoleActionDomainServices(IProject_SYS_RoleActionRepository repository)
        {
            this._repository = repository;
        }
    }
}
