﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using System;
namespace Company.Project.Domain.MainModule
{
    public class Project_SYS_ProvinceDomainServices : IProject_SYS_ProvinceDomainServices
    {
        private readonly IProject_SYS_ProvinceRepository _repository;

        public Project_SYS_ProvinceDomainServices(IProject_SYS_ProvinceRepository repository)
        {
            this._repository = repository;
        }
        
    }
}
