﻿using System;
using System.Web;
using Newtonsoft.Json;
using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;
using Z.Nlayer.Utility;
using Z.Nlayer.Utility.Encrypt;

namespace Company.Project.Domain.MainModule
{
    public class Project_Manager_LogDomainServices : IProject_Manager_LogDomainServices
    {
        private readonly IProject_Manager_LogRepository _repository;

        public Project_Manager_LogDomainServices(IProject_Manager_LogRepository repository)
        {
            this._repository = repository;
        }

        public void Log(string title, string Contents, LoginUserDTO mydto = null)
        {
            var dto = mydto ?? JsonConvert.DeserializeObject<LoginUserDTO>(JsonConvert.SerializeObject(Common.ReadSimpleLoginUser()));
            if (dto.ID == Guid.Empty)
            {
                throw new Exception("登录过期,日志写入失败。请重新登录!");
            }
            _repository.Add(new Project_Manager_Log()
            {
                Contents = Contents,
                CreateTime = DateTime.Now,
                ID = Guid.NewGuid(),
                IsDeleted = false,
                UId = dto.ID,
                IpAddress = IPHelper.getIPAddr(),
                Titles = title
            });
        }
    }
}
