﻿using Company.Project.Domain.MainModule.Interface;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule
{
    public class Project_Manager_RoleDomainServices : IProject_Manager_RoleDomainServices
    {
        private readonly IProject_Manager_RoleRepository _repository;

        public Project_Manager_RoleDomainServices(IProject_Manager_RoleRepository repository)
        {
            this._repository = repository;
        }
    }
}
