﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_ModuleActionRepository : IRepository<Project_SYS_ModuleAction, EntityList<Project_SYS_ModuleAction>>, IDependencyDynamicService
    {
        Task<List<LoginModultActionDTO>> GetModultAction();
    }
}