﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_ModuleRepository : IRepository<Project_SYS_Module, EntityList<Project_SYS_Module>>, IDependencyDynamicService
    {
        Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId);
        Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId);

        Task<List<RecursiveModuleDTO>> GetModuleByRecursive(Guid? Id = null);
        Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows, ISpecification<Project_SYS_Module> specification,
            Dictionary<string, string> sort);

        Task<List<Project_SYS_Module>> GetLoginModule();
    }
}