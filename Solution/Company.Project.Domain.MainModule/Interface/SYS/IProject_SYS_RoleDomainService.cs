﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_RoleDomainServices : IDependencyDynamicService
    {
        Task AddRole(Project_SYS_Role model, List<Project_SYS_ModuleAction> moduleAction);
        Task<IEnumerable<string>> DelRole(IEnumerable<Guid> ids);
    }
}
