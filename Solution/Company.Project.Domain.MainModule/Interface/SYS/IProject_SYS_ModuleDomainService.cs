﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_ModuleDomainServices : IDependencyDynamicService
    {
        Task<string> GetLoginRoleMenu(LoginUserDTO loginUser);
        Task AddModule(Project_SYS_Module item);
        Task<IEnumerable<string>> DelModule(IEnumerable<Guid> delId);
    }
}
