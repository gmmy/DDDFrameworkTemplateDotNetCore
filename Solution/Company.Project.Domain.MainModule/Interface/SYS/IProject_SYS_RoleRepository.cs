﻿using System.Collections.Generic;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Z.Nlayer.Infrastructure.Repository;
using System;
using System.Threading.Tasks;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_RoleRepository : IRepository<Project_SYS_Role, EntityList<Project_SYS_Role>>, IDependencyDynamicService
    {
        Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, ISpecification<Project_SYS_Role> specification,
            Dictionary<string, string> sort);
        Task<List<RoleListDTO>> LoadMangerRoleList(Guid id);

        Task<Project_SYS_Role> GetRoleAndAction(Guid id);
    }
}