﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Infrastructure.Common.Enums;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_PhoneMessageRecordRepository : IRepository<Project_SYS_PhoneMessageRecord, EntityList<Project_SYS_PhoneMessageRecord>>, IDependencyDynamicService
    {
        Task<bool> CheckCodeValidity(string phone, string code, Enum_PhoneMessageSendType sendType,
            Enum_PhoneMessageSendUserType sendUserType, string openId, Guid? userId);
    }
}