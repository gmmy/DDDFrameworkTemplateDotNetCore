﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_WeChatSettingDomainServices : IDependencyDynamicService
    {
        void ModifyModel(Project_SYS_WeChatSetting item);
    }
}
