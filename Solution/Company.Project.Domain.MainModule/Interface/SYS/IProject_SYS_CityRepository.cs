﻿using System.Collections.Generic;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_CityRepository : IRepository<Project_SYS_City, EntityList<Project_SYS_City>>, IDependencyDynamicService
    {
    }
}