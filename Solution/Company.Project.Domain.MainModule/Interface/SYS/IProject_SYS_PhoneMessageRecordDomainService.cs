﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Infrastructure.Common.Enums;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_PhoneMessageRecordDomainServices : IDependencyDynamicService
    {
        Task<string> SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType,
            Enum_PhoneMessageSendUserType sendUserType, string sendOpenId, string sendIpAddr, Guid? sendUserId);
    }
}
