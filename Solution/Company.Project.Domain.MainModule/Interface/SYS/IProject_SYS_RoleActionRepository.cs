﻿using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_SYS_RoleActionRepository : IRepository<Project_SYS_RoleAction, EntityList<Project_SYS_RoleAction>>, IDependencyDynamicService
    {

    }
}