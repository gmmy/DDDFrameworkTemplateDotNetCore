﻿using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Z.Nlayer.Infrastructure.Repository;
using System;
using System.Collections.Generic;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_Manager_RoleRepository : IRepository<Project_Manager_Role, EntityList<Project_Manager_Role>>, IDependencyDynamicService
    {
       
    }
}