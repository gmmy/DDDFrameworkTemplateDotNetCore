﻿using System;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Z.Nlayer.Infrastructure.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_Manager_BaseInfoRepository : IRepository<Project_Manager_BaseInfo, EntityList<Project_Manager_BaseInfo>>, IDependencyDynamicService
    {
        Task<EntityList<BaseInfoDTO>> GetBaseInfoList(int page, int rows, ISpecification<Project_Manager_BaseInfo> specification,
             Dictionary<string, string> sort);

        Task<Project_Manager_BaseInfo> GetLoginUser(ISpecification<Project_Manager_BaseInfo> specification);
        Task<bool> ExistsRoleByRoleId(IEnumerable<Guid> ids);
    }
}