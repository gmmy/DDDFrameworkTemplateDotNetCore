﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model.DTO.Manager;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_Manager_LogDomainServices : IDependencyDynamicService
    {
        void Log(string title, string Contents, LoginUserDTO mydto = null);
    }
}
