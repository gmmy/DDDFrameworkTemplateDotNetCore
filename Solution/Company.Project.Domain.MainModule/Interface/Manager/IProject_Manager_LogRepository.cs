﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Z.Nlayer.Infrastructure.Repository;

namespace Company.Project.Domain.MainModule.Interface
{
    public interface IProject_Manager_LogRepository : IRepository<Project_Manager_Log, EntityList<Project_Manager_Log>>, IDependencyDynamicService
    {
        Task<EntityList<LogDTO>> GetLogList(int page, int rows, ISpecification<Project_Manager_Log> specification,
            Dictionary<string, string> sort);
        Task<List<dynamic>> ManagerLogChart(ISpecification<Project_Manager_Log> specification);
    }
}