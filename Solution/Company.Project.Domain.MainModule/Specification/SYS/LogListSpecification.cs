﻿using System;
using System.Linq.Expressions;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule.Specification
{
    public class LogListSpecification : Specification<Project_Manager_Log>
    {
        private readonly DateTime? _searchStartCreateTime; 
        private DateTime? _searchEndCreateTime;
        private readonly string _searchText;

        public LogListSpecification(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText
            )
        {
            _searchStartCreateTime = searchStartCreateTime;
            _searchEndCreateTime = searchEndCreateTime;
            _searchText = searchText;
        }

        public override Expression<Func<Project_Manager_Log, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<Project_Manager_Log>();
            where = where.And(x => !x.IsDeleted);
            if (_searchStartCreateTime != null)
            {
                where = where.And(x => x.CreateTime >= _searchStartCreateTime);
            }
            if (_searchEndCreateTime != null)
            {
                _searchEndCreateTime = _searchEndCreateTime.Value.AddDays(1);
                where = where.And(x => x.CreateTime < _searchEndCreateTime);
            }
            if (!string.IsNullOrEmpty(_searchText))
            {
                where = where.And(x => x.UserBaseInfo.LoginName.Contains(_searchText) || x.Titles.Contains(_searchText) || x.Contents.Contains(_searchText));
            }
            return where;
        }
    }
}
