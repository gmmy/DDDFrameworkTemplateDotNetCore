﻿using System;
using System.Linq.Expressions;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule.Specification
{
    public class RoleListSpecification : Specification<Project_SYS_Role>
    {
        //private string _name;

        public RoleListSpecification(
            //string name
            )
        {
            //_name = name;
        }

        public override Expression<Func<Project_SYS_Role, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<Project_SYS_Role>();
            where = where.And(x => !x.IsDeleted);
            //if (string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.RoleName.Contains(_name));
            //}
            return where;
        }
    }
}
