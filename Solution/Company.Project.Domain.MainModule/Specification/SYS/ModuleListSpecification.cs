﻿using System;
using System.Linq.Expressions;
using Z.Nlayer.Domain.Base;
using Company.Project.Domain.Model;

namespace Company.Project.Domain.MainModule.Specification
{
    public class ModuleListSpecification : Specification<Project_SYS_Module>
    {
        private Guid _pid;

        public ModuleListSpecification(
            Guid pid
            )
        {
            _pid = pid;
        }

        public override Expression<Func<Project_SYS_Module, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<Project_SYS_Module>();
            where = where.And(x => !x.IsDeleted && x.PID== _pid);
            return where;
        }
    }
}
