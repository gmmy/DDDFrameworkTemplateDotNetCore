﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Company.Project.Infrastructure.Common.Enums;
using Z.Nlayer.Domain.Base;

namespace Company.Project.Domain.Model
{
    public class Project_SYS_City : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 所属省份编号
        /// </summary>
        [DisplayName(@"所属省份编号")]
        public Guid ProvinceId { get; set; }

        /// <summary>
        /// 城市名称
        /// </summary>
        [DisplayName(@"城市名称")]
        [MaxLength(50)]
        public string CityName { get; set; }
        public virtual string PyCityName { get; set; }

        /// <summary>
        /// 城市编码
        /// </summary>
        [DisplayName(@"城市编码")]
        [MaxLength(50)]
        public string CityCode { get; set; }
    }
}