﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Company.Project.Infrastructure.Common.Enums;
using Z.Nlayer.Domain.Base;

namespace Company.Project.Domain.Model
{
    public class Project_SYS_Province : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName(@"名称")]
        [MaxLength(50)]
        public string ProvinceName { get; set; }

        /// <summary>
        /// 省市编码
        /// </summary>
        [DisplayName(@"省市编码")]
        [MaxLength(50)]
        public string ProvinceCode { get; set; }
        public virtual string PyProvinceName { get; set; }
    }
}