﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Company.Project.Domain.Model;
using Company.Project.Domain.Model.DTO;
using Company.Project.Domain.Model.DTO.Manager;
using Microsoft.Extensions.DependencyInjection;

namespace Company.Project.Domain.Model.DTO
{
    public static class MapperInitializeFactory
    {
        public static void MapperInitialize(this IServiceCollection services)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Project_Manager_BaseInfo, LoginUserDTO>();
                x.CreateMap<Project_SYS_RoleAction, RoleActionDTO>();
            });
        }
    }
}