﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace EFCodeCreater
{
    public partial class Form1 : Form
    {

        private bool isAllComplete = true;

        private IContainer components = null;

        private Button button1;

        private Label label1;

        private Label label2;

        private Label label3;

        private TextBox textBox1;

        private TextBox textBox2;

        private TextBox textBox3;
        public string dbtypestr;

        public Form1()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dbtypestr = this.dbtype.Text;

            if (this.textBox1.Text == "" || this.textBox2.Text == ""  || this.comboBox1.Text == "")
            {
                MessageBox.Show(string.Concat(new string[]
                {
                    "请输入/选择\n",
                    (this.textBox1.Text == "") ? "服务器名称\n" : "",
                    (this.textBox2.Text == "") ? "登录名\n" : "",
                    (this.comboBox1.Text == "") ? "数据库\n" : ""
                }));
            }
            else
            {
                switch (this.dbtype.Text)
                {
                    case "sql":
                        try
                        {
                            SqlConnection sqlConnection = new SqlConnection
                            {
                                ConnectionString = string.Format("data source={0};Initial Catalog={3};User ID={1};Password={2}", new object[]
                                {
                                    this.textBox1.Text,
                                    this.textBox2.Text,
                                    this.textBox3.Text,
                                    this.comboBox1.Text
                                })
                            };
                            sqlConnection.Open();
                            new Form2
                            {
                                TrainConnection = sqlConnection,
                                databsename = this.comboBox1.Text,
                                dbtype = this.dbtype.Text
                            }.Show();
                            base.Hide();
                            File.WriteAllText((AppDomain.CurrentDomain.BaseDirectory + "/eflogin.txt"), textBox1.Text + "\r\n" + textBox2.Text + "\r\n" + textBox3.Text);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case "mysql":
                        try
                        {
                            MySqlConnection mysqlConnection = new MySqlConnection
                            {
                                ConnectionString = string.Format("data source={0};Initial Catalog={3};User ID={1};Password={2}", new object[]
                                {
                                    this.textBox1.Text,
                                    this.textBox2.Text,
                                    this.textBox3.Text,
                                    this.comboBox1.Text
                                })
                            };
                            mysqlConnection.Open();
                            new Form2
                            {
                                MySqlTrainConnection = mysqlConnection,
                                databsename = this.comboBox1.Text,
                                dbtype = this.dbtype.Text
                            }.Show();
                            base.Hide();
                            File.WriteAllText((AppDomain.CurrentDomain.BaseDirectory + "/eflogin.txt"), textBox1.Text + "\r\n" + textBox2.Text + "\r\n" + textBox3.Text);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        break;
                }
            }
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            dbtypestr = this.dbtype.Text;
            if (this.isAllComplete)
            {
                this.isAllComplete = false;
                if (this.textBox1.Text == "" || this.textBox2.Text == "")
                {
                    this.isAllComplete = true;
                    MessageBox.Show("请输入服务器名称和登录名");
                }
                else
                {
                    Task task = Task.Factory.StartNew(delegate
                    {
                        this.LoadCombBox();
                    });
                }
            }
        }

        private void LoadCombBox()
        {
            switch (dbtypestr)
            {
                case "sql":
                    LoadCombBoxForSql();
                    break;
                case "mysql":
                    LoadCombBoxForMysql();
                    break;
            }
        }
        public void LoadCombBoxForSql()
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection
                {
                    ConnectionString = string.Format("data source={0};User ID={1};Password={2}", this.textBox1.Text, this.textBox2.Text, this.textBox3.Text)
                };
                sqlConnection.Open();
                string selectCommandText = "Select name FROM Master..SysDatabases order by name";
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommandText, sqlConnection);
                DataSet ds = new DataSet();
                sqlDataAdapter.Fill(ds, "ds1");
                sqlConnection.Close();
                base.Invoke(new MethodInvoker(delegate
                {
                    this.comboBox1.DataSource = ds.Tables[0];
                    this.comboBox1.ValueMember = "name";
                }));
            }
            catch (Exception ee)
            {
                this.isAllComplete = true;
                base.Invoke(new MethodInvoker(delegate
                {
                    MessageBox.Show(ee.Message);
                }));
            }
        }
        public void LoadCombBoxForMysql()
        {
            try
            {
                MySqlConnection sqlConnection = new MySqlConnection
                {
                    ConnectionString = string.Format("data source={0};User ID={1};Password={2}", this.textBox1.Text, this.textBox2.Text, this.textBox3.Text)
                };

                sqlConnection.Open();
                string selectCommandText = "show databases;";//获取所有数据库
                MySqlDataAdapter sqlDataAdapter = new MySqlDataAdapter(selectCommandText, sqlConnection);
                DataSet ds = new DataSet();
                sqlDataAdapter.Fill(ds, "ds1");
                sqlConnection.Close();
                base.Invoke(new MethodInvoker(delegate
                {
                    this.comboBox1.DataSource = ds.Tables[0];
                    this.comboBox1.ValueMember = "Database";
                }));
            }
            catch (Exception ee)
            {
                this.isAllComplete = true;
                base.Invoke(new MethodInvoker(delegate
                {
                    MessageBox.Show(ee.Message);
                }));
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "/eflogin.txt"))
            {
                string text =File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/eflogin.txt").Replace("\r\n", "|");
                if (!string.IsNullOrEmpty(text))
                {
                    string[] array = text.Split(new char[]
                    {
                    '|'
                    });
                    this.textBox1.Text = array[0];
                    this.textBox2.Text = array[(array.Length > 0) ? 1 : 0];
                    this.textBox3.Text = array[(array.Length > 1) ? 2 : 0];
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.dbtype = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(89, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "登录";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "服务器名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "登录名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "密码";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(88, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(195, 21);
            this.textBox1.TabIndex = 4;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(88, 45);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(195, 21);
            this.textBox2.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(88, 74);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(195, 21);
            this.textBox3.TabIndex = 6;
            this.textBox3.UseSystemPasswordChar = true;
            // 
            // dbtype
            // 
            this.dbtype.FormattingEnabled = true;
            this.dbtype.Items.AddRange(new object[] {
            "sql",
            "mysql"});
            this.dbtype.Location = new System.Drawing.Point(89, 101);
            this.dbtype.Name = "dbtype";
            this.dbtype.Size = new System.Drawing.Size(194, 20);
            this.dbtype.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "数据库连接类型";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(89, 128);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(194, 20);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.Click += new System.EventHandler(this.comboBox1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "数据库";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 213);
            this.Controls.Add(this.dbtype);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "登录数据库";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
