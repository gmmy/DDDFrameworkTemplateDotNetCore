using System;

namespace MyAddin
{
	public static class CodePath
	{
        public const string EF_Entity = "/DDD业务模块/Domain.Model/Entities/";

        public const string EF_DTO= "/DDD业务模块/Domain.Model/DTO/";

        public const string EF_Service = "/DDD业务模块/Application.Service/";

        public const string EF_IService = "/DDD业务模块/Application.Service/Interface/";

        public const string EF_DomainService = "/DDD业务模块/Domain.MainModule/";

	    public const string EF_DomainServiceSpecification = "/DDD业务模块/Domain.MainModule/Specification/";

        public const string EF_IDomainService = "/DDD业务模块/Domain.MainModule/Interface/";

        public const string EF_Repository = "/DDD业务模块/Infrastructure.Repository.Sqlserver/";

		public const string EF_OTHER = "/DDD业务模块/";

	    public const string EF_UI_Controller = "/DDD业务模块/AdminUI/";
    }
}
