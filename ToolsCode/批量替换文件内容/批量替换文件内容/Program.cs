﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace 批量替换文件内容
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("请输入原始项目名称");
            string scourekey = Console.ReadLine();
            Console.Write("请输入要替换的项目名称");
            string key = Console.ReadLine();
            Console.Write("请输入要替换的项目根目录");
            string path = Console.ReadLine();
            if (string.IsNullOrEmpty(key))
            {
                Console.Write("什么也没有发生....点击任意键关闭程序");
                Console.ReadKey();
            }
            else
            {
                var di = new DirectoryInfo(path);
                foreach (var f in di.GetDirectories())
                {
                    var fname = f.FullName
                        .Replace(scourekey + ".", key + ".").Replace(scourekey + "_", key + "_");
                    if (f.FullName != fname)
                    {
                        Directory.Move(f.FullName, fname);
                    }
                }
                di = new DirectoryInfo(path);
                changeFile(di, key, scourekey);
            }
        }
        public static string[] replaceexten=new string[] {".dll",".png",".jpg",".jpeg",".gif",".bmp",".exe",".ttf"};
        public static string[] outdirects = new string[] {"assets", "js", "Content", "Scripts"};
        public static void changeFile(DirectoryInfo di, string replacename,string scourekey)
        {
            if (string.IsNullOrEmpty(replacename))
            {
                
            }
            else
            {
                //替换Project
                foreach (var file in di.GetFiles())
                {
                    var irectorypath = file.DirectoryName + "\\";
                    var name = file.Name;
                    if (file.Extension == ".vspscc")
                    {
                        File.Delete(file.FullName);
                    }
                    else if (file.Extension == ".sln")
                    {
                        StreamReader sr = new StreamReader(file.FullName, EncodingType.GetType(file.FullName));
                        var newpath = name;
                        var text = sr.ReadToEnd()
                        .Replace(scourekey + ".", replacename + ".").Replace(scourekey + "_", replacename + "_");
                        newpath = name
                        .Replace(scourekey + ".", replacename + ".").Replace(scourekey + "_", replacename + "_");
                        sr.Dispose();
                        if (newpath != name)
                        {
                            File.Delete(file.FullName);
                        }
                        File.WriteAllText(irectorypath + newpath, text, Encoding.UTF8);
                    }
                    else if (!replaceexten.Contains(file.Extension))
                    {
                        StreamReader sr = new StreamReader(file.FullName, EncodingType.GetType(file.FullName));
                        var newpath = name;
                        var text = sr.ReadToEnd()
                        .Replace(scourekey + ".", replacename + ".").Replace(scourekey + "_", replacename + "_");

                        ;
                        newpath = name
                        .Replace(scourekey + ".", replacename + ".").Replace(scourekey + "_", replacename + "_");
                        sr.Dispose();
                        if (newpath != name)
                        {
                            File.Delete(file.FullName);
                        }
                        File.WriteAllText(irectorypath + newpath, text, Encoding.UTF8);
                    }
                }
                foreach (var dis in di.GetDirectories())
                {
                    if (dis.Name == "bin" || dis.Name == "obj")
                    {
                        Directory.Delete(dis.FullName, true);
                    }
                    else if(!outdirects.Contains(dis.Name))
                    {
                        changeFile(dis, replacename, scourekey);
                    }
                }
            }
        }
    }
    class EncodingType
    {
        /// <summary> 
        /// 给定文件的路径，读取文件的二进制数据，判断文件的编码类型 
        /// </summary> 
        /// <param name="FILE_NAME">文件路径</param> 
        /// <returns>文件的编码类型</returns> 
        public static System.Text.Encoding GetType(string FILE_NAME)
        {
            FileStream fs = new FileStream(FILE_NAME, FileMode.Open, FileAccess.Read);
            Encoding r = GetType(fs);
            fs.Close();
            return r;
        }

        /// <summary> 
        /// 通过给定的文件流，判断文件的编码类型 
        /// </summary> 
        /// <param name="fs">文件流</param> 
        /// <returns>文件的编码类型</returns> 
        public static System.Text.Encoding GetType(FileStream fs)
        {
            byte[] Unicode = new byte[] { 0xFF, 0xFE, 0x41 };
            byte[] UnicodeBIG = new byte[] { 0xFE, 0xFF, 0x00 };
            byte[] UTF8 = new byte[] { 0xEF, 0xBB, 0xBF }; //带BOM 
            Encoding reVal = Encoding.Default;

            BinaryReader r = new BinaryReader(fs, System.Text.Encoding.Default);
            int i;
            int.TryParse(fs.Length.ToString(), out i);
            byte[] ss = r.ReadBytes(i);
            if (IsUTF8Bytes(ss) || (ss[0] == 0xEF && ss[1] == 0xBB && ss[2] == 0xBF))
            {
                reVal = Encoding.UTF8;
            }
            else if (ss[0] == 0xFE && ss[1] == 0xFF && ss[2] == 0x00)
            {
                reVal = Encoding.BigEndianUnicode;
            }
            else if (ss[0] == 0xFF && ss[1] == 0xFE && ss[2] == 0x41)
            {
                reVal = Encoding.Unicode;
            }
            r.Close();
            return reVal;

        }

        /// <summary> 
        /// 判断是否是不带 BOM 的 UTF8 格式 
        /// </summary> 
        /// <param name="data"></param> 
        /// <returns></returns> 
        private static bool IsUTF8Bytes(byte[] data)
        {
            int charByteCounter = 1;
            //计算当前正分析的字符应还有的字节数 
            byte curByte; //当前分析的字节. 
            for (int i = 0; i < data.Length; i++)
            {
                curByte = data[i];
                if (charByteCounter == 1)
                {
                    if (curByte >= 0x80)
                    {
                        //判断当前 
                        while (((curByte <<= 1) & 0x80) != 0)
                        {
                            charByteCounter++;
                        }
                        //标记位首位若为非0 则至少以2个1开始 如:110XXXXX...........1111110X　 
                        if (charByteCounter == 1 || charByteCounter > 6)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    //若是UTF-8 此时第一位必须为1 
                    if ((curByte & 0xC0) != 0x80)
                    {
                        return false;
                    }
                    charByteCounter--;
                }
            }
            if (charByteCounter > 1)
            {
                throw new Exception("非预期的byte格式");
            }
            return true;
        }
    }
}
